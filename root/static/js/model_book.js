//noinspection JSUnresolvedFunction
/**
 * Created by ishan on 7/1/17.
 */
var urlRoot = bookUrl();
TastypieModel = Backbone.Model.extend({
    // url creation referenced from http://stackoverflow.com/a/8277954/1641882
    urlRoot: urlRoot,
    // Overriding url to make sure that it always ends up with a slash '/'
    url: function () {
        if(this.has('resource_uri')) {
            return this.get('resource_uri');
        }
        if(this.has('id')) {
            return _.result(this, 'urlRoot', urlRoot) + this.get('id') + '/';
        }
        return _.result(this, 'urlRoot', urlRoot);
    }
});

Book = TastypieModel.extend({
    // Constructor
    initialize: function() {
        console.log('a new Book');
        // Listening for changes
        this.on('change', function() {
            console.log('Book Changed');
        });
        this.on('change:title', function() {
            console.log('The title attribute has changed');
        });
        // Listening for invalid changes
        this.on('invalid', function(model, error) {
            console.log('**Validation Error : ' + error + '**');
        });
    },
    defaults: {
        title: 'Book Title',
        author: 'No One'
    },
    validate: function(attrs){
        if(attrs.year && attrs.year < 1455) {
            return 'Year must be after 1455';
        }
        if(!attrs.title) {
            return 'A title must be provided';
        }
    },
    // parse works with save() and fetch()
    parse: function (response, xhr) {
        response.bookType = 'ebook';
        return response;
    },
    printDetails: function() {
        var representation = this.get('title') + ' by ' + this.get('author');
        if(this.has('year')) {
            representation += ' in ' + this.get('year')
        }
        console.log(representation);
    }
});