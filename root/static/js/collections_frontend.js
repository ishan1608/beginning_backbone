/**
 * Created by ishan on 7/1/17.
 */
var bookOne = new Book({title: 'THE VOYAGE OF THE DAWN TREADER', author: 'C.S. Lewis'});
var bookTwo = new Book({title: 'THE FOUR LOVES', author: 'C.S. Lewis'});
var library = new Library([bookOne, bookTwo]);
console.log('length: ' + library.length + ' size(): ' + library.size());
var bookThree = new Book({title: 'THE FIVE LOVE LANGUAGES', author: 'Gary Chapman'});
library.add(bookThree);
console.log('library has ' + library.size() + ' books');
var bookFour = new Book({title: 'FAILURE IS NOT AN OPTION', author: 'Gene Kranz'});
var bookFive = new Book({title: 'WICKED BUGS', author: 'Amy Stewart'});
library.add([bookFour, bookFive]);
console.log('library has ' + library.size() + ' books');
library.add(bookOne, {merge: true});
console.log('library has ' + library.size() + ' books');
var bookSix = new Book({title: 'LETTERS TO A YOUNG SCIENTIST', author: 'Edward O Wilson'});
library.push(bookSix);
console.log('library has ' + library.size() + ' books');
var bookSeven = new Book({title: 'THE FAULT IN OUR STARS', author: 'John Green'});
library.unshift(bookSeven);
console.log('library has ' + library.size() + ' books');

library.remove(bookOne);
library.remove([bookTwo, bookThree]);
library.pop();
library.shift();
console.log('library has ' + library.size() + ' books');
library.reset([bookOne]);
console.log('library has ' + library.size() + ' books');
library.reset();
console.log('library has ' + library.size() + ' books');
library.set([bookOne]);
console.log('library has ' + library.size() + ' books');
library.set([bookSix, bookSeven], {remove: false});
console.log('library has ' + library.size() + ' books');
library.get(_.result(bookSeven, 'cid')).printDetails();
library.at(2).printDetails();
library.forEach(function (model) {
    model.printDetails();
});
