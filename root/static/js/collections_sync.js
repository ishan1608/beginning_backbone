/**
 * Created by ishan on 7/1/17.
 */
var library = new Library();
library.fetch({
    success: function (data) {
        console.log('Got data');
        console.log(data);
    },
    error: function (error) {
        console.log('Something went wrong');
        console.log(error);
    }
});
var book = new Book({title: 'THE FIVE LOVE LANGUAGES', author: 'Gary Chapman'});
library.create(book, {
    success: function (model) {
        console.log('Model created');
        book.destroy({
            success: function (model) {
                console.log('Model deleted');
            },
            error: function (error) {
                console.log('Error deleting model');
            }
        });
    },
    error: function (error) {
        console.log('Error creating model');
        console.log(error);
    }
});
