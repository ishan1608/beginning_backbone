//noinspection JSUnresolvedFunction
/**
 * Created by ishan on 7/1/17.
 */
var urlRoot = bookUrl();
var TastypieCollection = Backbone.Collection.extend({
    parse: function (response, xhr) {
        this.recent_meta = response.meta || {};
        return response.objects || response;
    }
});
resp = {};
var Library = TastypieCollection.extend({
    url: urlRoot,
    model: Book,
    initialize: function () {
        console.log('Creating a new library collection');
        this.on('remove', function (removedModel, models, options) {
            console.log('element removed at ' + options.index);
        });
    },
    parse: function (response, xhr) {
        // Any parsing to the collection can be done here
        // response.objects is an array of all models
        for (var i = response.objects.length - 1; i >= 0; i--) {
            response.objects[i].collection = true;
        }
        return TastypieCollection.prototype.parse.call(this, response, xhr);
    }
});