//noinspection JSUnresolvedFunction
/**
 * Created by ishan on 7/1/17.
 */
var thisBook = new Book({title: 'THE VOYAGE OF THE DAWN TREADER', author: 'C.S. Lewis', year: 1952});
thisBook.save(thisBook.attributes, {
    success: function (model, response, options) {
        console.log('Model saved');
        console.log('Id: ' + thisBook.get('id'));
        thisBook.printDetails();

        thisBook.set('title', 'The Voyage of The Dawn Treader');
        thisBook.save(thisBook.attributes, {
            success: function (model, response, options) {
                console.log('Model saved');
                // Must be done after making a change on the server to see the effects
                thisBook.fetch({
                    success: function (model, response, options) {
                        console.log('Fetch success');
                        thisBook.printDetails();

                            // Must be done only when the object is to be destroyed from the server
                            thisBook.destroy({
                                success: function (model, response, options) {
                                    console.log('Destroy success');
                                },
                                error: function (model, response, options) {
                                    console.log('Destroy error');
                                },
                                wait: true
                            });
                    },
                    error: function (model, response, options) {
                        console.log('Fetch error');
                        thisBook.printDetails();
                    }
                });
            },
            error: function (model, response, options) {
                console.log('Failed to save model');
            }
        });
    },
    error: function (model, xhr, options) {
        console.log('Failed to save model');
    }
});
