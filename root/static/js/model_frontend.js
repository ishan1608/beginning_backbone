/**
 * Created by ishan on 7/1/17.
 */
var thisBook = new Book();
thisBook.printDetails();
thisBook.set('title', 'THE FOUR LOVES');
thisBook.set('title', 'The Four Loves', {silent: true});

thisBook = new Book({title: 'THE VOYAGE OF THE DAWN TREADER', author: 'C.S. Lewis'});
thisBook.printDetails();


console.log(thisBook.has('year'));
thisBook.set('year', 1952);
console.log(thisBook.has('year'));
console.log(thisBook.get('year'));
thisBook.unset('year');
console.log(thisBook.get('year'));

console.log('Check year change: ' + thisBook.set('year', 1999, {validate: true}));
console.log('Check if title was removed: ' + thisBook.unset('title', {validate: true}));
console.log('Is model valid: ' + thisBook.isValid());
console.log('Check year change: ' + thisBook.set('year', 1952));
console.log('Is model valid: ' + thisBook.isValid());
console.log(thisBook.attributes);