/**
 * Created by ishan on 7/1/17.
 */
var bookOne = new Book({title: 'THE VOYAGE OF THE DAWN TREADER', author: 'C.S. Lewis'});
var bookTwo = new Book({title: 'THE FOUR LOVES', author: 'C.S. Lewis'});
var library = new Library([bookOne, bookTwo]);
var bookThree = new Book({title: 'THE FIVE LOVE LANGUAGES', author: 'Gary Chapman'});
library.add(bookThree);
var bookFour = new Book({title: 'FAILURE IS NOT AN OPTION', author: 'Gene Kranz'});
var bookFive = new Book({title: 'WICKED BUGS', author: 'Amy Stewart'});
library.add([bookFour, bookFive]);
var bookSix = new Book({title: 'LETTERS TO A YOUNG SCIENTIST', author: 'Edward O Wilson'});
library.push(bookSix);
var bookSeven = new Book({title: 'THE FAULT IN OUR STARS', author: 'John Green'});
library.unshift(bookSeven);
console.log('Original order:');
library.forEach(function (book) {
    book.printDetails();
});
var librarySortedByTitle = library.sortBy(function (book) {
    return book.get('title');
});
console.log('Sorted by Title:');
librarySortedByTitle.forEach(function (book) {
    book.printDetails();
});

var LibrarySorted = Library.extend({
    comparator: function (a, b) {
        return a.get('title') < b.get('title') ? -1: 1;
    }
});

bookOne = new Book({title: 'THE VOYAGE OF THE DAWN TREADER', author: 'C.S. Lewis'});
bookTwo = new Book({title: 'THE FOUR LOVES', author: 'C.S. Lewis'});
bookThree = new Book({title: 'THE FIVE LOVE LANGUAGES', author: 'Gary Chapman'});
bookFour = new Book({title: 'FAILURE IS NOT AN OPTION', author: 'Gene Kranz'});
bookFive = new Book({title: 'WICKED BUGS', author: 'Amy Stewart'});
bookSix = new Book({title: 'LETTERS TO A YOUNG SCIENTIST', author: 'Edward O Wilson'});
bookSeven = new Book({title: 'THE FAULT IN OUR STARS', author: 'John Green'});
library = new LibrarySorted([bookOne, bookTwo, bookThree, bookFour, bookFive, bookSix, bookSeven]);
console.log('Original order:');
library.forEach(function (book) {
    book.printDetails();
});
library.at(0).set('title', 'THE SHARK\'S PAINTBRUSH');
library.at(0).set('author', 'Jay Harman');
console.log('Disordered:');
library.forEach(function (book) {
    book.printDetails();
});
library.sort();
console.log('Ordered (Forced):');
library.forEach(function (book) {
    book.printDetails();
});
libraryShuffled = library.shuffle();
console.log('Shuffled:');
libraryShuffled.forEach(function (book) {
    book.printDetails();
});
console.log('Authors:');
var authors = library.pluck('author');
authors.forEach(function (author) {
    console.log(author);
});
var results = library.where({author: 'C.S. Lewis'});
console.log('Found: ' + results.length + ' matches');
console.log('Book count:');
var libraryGroupByAuthor = library.groupBy('author');
for (var author in libraryGroupByAuthor) {
    if (libraryGroupByAuthor.hasOwnProperty(author)) {
        console.log(author + ' : ' + libraryGroupByAuthor[author].length);
    }
}
