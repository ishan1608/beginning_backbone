/**
 * Created by ishan on 7/1/17.
 */
var book = new Book({title: 'THE FIVE LOVE LANGUAGES', author: 'Gary Chapman'});
BookView = Backbone.View.extend({
    initialize: function () {
        console.log('View created');
    },
    render: function () {
        this.$el.html('Book Title: ' + this.model.get('title'));
        return this;
    }
});
var bookView = new BookView({
    model: book,
    el: '#book-view'
});
bookView.render();

BookView2 = Backbone.View.extend({
    initialize: function () {
        this.render();
        $('#book-view-2-container').append(this.el);
    },
    render: function () {
        console.log('render of 2 called');
        this.$el.html('Book Title: ' + this.model.get('title'));
        return this;
    }
});
new BookView2({
    model: book,
    tagName: 'p',
    className: 'title',
    id: 'book-view-2'
});
