from django.conf.urls import url, include
from .views import index
import api_urls

urlpatterns = [
    url(r'^$', index, name='index'),

    # REST APIs
    url(r'^api/', include(api_urls)),
]
