from __future__ import unicode_literals

from django.db import models


class Book(models.Model):
    author = models.CharField(max_length=1024, null=True, blank=True)
    title = models.CharField(max_length=1024, null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return '{} - {}'.format(self.title, self.author)
